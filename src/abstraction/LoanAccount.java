package abstraction;

public class LoanAccount implements Account {

    double loanAmount;

    public LoanAccount(double loanAmount)
    {
        this.loanAmount=loanAmount;
        System.out.println("Loan account created");
    }

    @Override
    public void deposit(double amt) {
       loanAmount+=amt;
        System.out.println(amt+"RS debited to your loan account");
    }

    @Override
    public void withdraw(double amt) {
        if(amt<=loanAmount) {
            loanAmount-= amt;
            System.out.println(amt + "RS withdraw from your loan  account");
        }else {
            System.out.println("loan Account balance is insufficient");
        }
    }

    @Override
    public void checkBalance() {
        System.out.println("loan account Balance is : "+loanAmount);
    }
}
