package abstraction;

public class SavingAccount implements Account {
    double accountBalance;

    public SavingAccount(double accountBalance)
    {
        this.accountBalance=accountBalance;
        System.out.println("Saving account created");
    }
    public void deposit(double amt) {

        accountBalance+=amt;
        System.out.println(amt+"RS credited to your account");
    }

    @Override
    public void withdraw(double amt) {
        if(amt<=accountBalance) {
            accountBalance -= amt;
            System.out.println(amt + "RS withdraw from your account");
        }else {
            System.out.println("Account balance is insufficient");
        }
    }

    @Override
    public void checkBalance() {
          System.out.println("Saving account Blanace is : "+accountBalance);
    }
}
