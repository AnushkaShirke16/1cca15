package abstraction;
import java.util.Scanner;
public class MainApp  {
    public static void main(String [] args)
    {
        Scanner S1=new Scanner(System.in);
        System.out.println("Select Account Type");
        System.out.println("1.Saving \n 2. Loan");
        int accType= S1.nextInt();

        System.out.println("Enter Account opening amount:");
         double accBalance=S1.nextDouble();

         AccountFactory factory=new AccountFactory();
          Account accRef=factory.createAccount(accType,accBalance);


         boolean status=true;

         while(status)
         {
             System.out.println("Select Transaction mode");
             System.out.println("1.Deposit \n 2.Withdraw \n 3. check Balance");
             int choice=S1.nextInt();

             if(choice==1)
             {
                 System.out.println("Enter Amount :");
                 double amt=S1.nextDouble();
                 accRef.deposit(amt);
             } else if (choice==2) {
                 System.out.println("Enter Amount :");
                 double amt=S1.nextDouble();
                 accRef.withdraw(amt);

             } else if (choice==3) {
                 accRef.checkBalance();

             }
             else {
                 status=false;
             }
         }



    }
}
